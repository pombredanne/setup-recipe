=====================
Setup Buildout Recipe
=====================

Runs the ``setup.py`` in all specified directories, using custom interpreter, e.g. the one generated by zc.buildout.egg
recipe. Its intended to be used to run any commands that require libraries (eggs) from buildout.
Any additional command line arguments are passed to the ``setup.py``. Setup is always run in working directory where it
resides.

Parameters

* develop - List of directories, where ``setup.py`` file reside. Typically ``${buildout:develop}`` to list all developed apps.
* use-interpreter - Name of python interpreter to use. Optional, when not specified, system interpreter is used.
* command - Command line argument for the ``setup.py`` (or other defined script). Optional.
* script - Name of the executed script. Defaults to ``setup.py``.

Typical buildout.cfg snippet::

    [python]
    recipe = zc.recipe.egg
    interpreter = python
    eggs = ${buildout:eggs}

    [test]
    recipe = setup-recipe
    use-interpreter = ${python:interpreter}
    develop = ${buildout:develop}
    command = test

Running ``./bin/test`` will then run ``./bin/python setup.py test`` for every developed project.

Adding arguments ``./bin/test sdist register upload`` will then test, build and publish all apps to cheeseshop.
Since you can omit implicit command, you can have generic script ``setup`` and call it with whatever arguments you wish.
